import numpy as np

class Ant:
    def __init__(self, destinations, distances):
        self.antDistances = distances;
        self.remainingPoints = np.arange(destinations);
        self.checkedPoints = [];
        self.currentPosition = 0;