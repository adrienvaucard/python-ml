import math

from Material import *
from Arrow import *

scorpioData = {
    'armLength': {
        'min': 4,
        'max': 6,
        'digits': 3
    },
    'armBase': {
        'min': 0.03,
        'max': 0.2,
        'digits': 3
    },
    'armHeight': {
        'min': 0.03,
        'max': 0.2,
        'digits': 3
    },
    'stringLength': {
        'min': 1.5,
        'max': 4,
        'digits': 3
    },
    'angle': {
        'min': 0,
        'max': 90,
        'digits': 1
    }
}

class Scorpio:
    def __init__(self, arrow, material, armLength, armBase, armHeight, stringLength, angle):
        self.arrow = arrow;
        self.material = material;
        self.armLength = armLength;
        self.armBase = armBase;
        self.armHeight = armHeight;
        self.stringLength = stringLength;
        self.angle = angle;

        self.emptyLength = (1/2) * math.sqrt(math.pow(self.armLength, 2) - math.pow(self.stringLength, 2));
        self.movementLength = arrow.length - self.emptyLength;
        self.I = (self.armBase * math.pow(self.armHeight, 3)) / 12;

        # Determiner max force
        self.K = (1/3) * (material.E / (1 - (2 * material.v)));
        self.F = self.K * self.movementLength;
        self.force = (self.F * math.pow(self.armLength, 3)) / (48 * material.E * self.I);

def getRandomScorpio(arrow):
    scorpio = Scorpio(
        arrow,
        materials[randrange(0, len(materials))],
        round(uniform(scorpioData['armLength']['min'], scorpioData['armLength']['max']), scorpioData['armLength']['digits']),
        round(uniform(scorpioData['armBase']['min'], scorpioData['armBase']['max']), scorpioData['armBase']['digits']),
        round(uniform(scorpioData['armHeight']['min'], scorpioData['armHeight']['max']), scorpioData['armHeight']['digits']),
        round(uniform(scorpioData['stringLength']['min'], scorpioData['stringLength']['max']), scorpioData['stringLength']['digits']),
        round(uniform(scorpioData['angle']['min'], scorpioData['angle']['max']), scorpioData['angle']['digits']),
    );

    return scorpio;
        