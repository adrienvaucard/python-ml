import math

from Material import *
from random import randrange, uniform

arrowData = {
    'length': {
        'min': 0.6,
        'max': 0.8,
        'digits': 3
    },
    'diameter': {
        'min': 0.005,
        'max': 0.03,
        'digits': 3
    }
}

class Arrow:
    def __init__(self, material, length, diameter):
        self.material = material;
        self.length = length;
        self.diameter = diameter;
        self.mass = self.material.p * (math.pi * math.pow((self.diameter/2), 2)) * self.length;


def getRandomArrow():
    arrow = Arrow(
        materials[randrange(0, len(materials))],
        round(uniform(arrowData['length']['min'], arrowData['length']['max']), arrowData['length']['digits']),
        round(uniform(arrowData['diameter']['min'], arrowData['diameter']['max']), arrowData['diameter']['digits']),
    );

    return arrow;
        