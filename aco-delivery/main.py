import random as rand
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pygal

from Ant import *

NODES_NB=50;
MAX_DISTANCE=100;
EVAPORATION_RATE=0.01;
POPULATION=1000;
GRADE_MULTIPLIER=5.0;
PHEROMONE_RATE=0.5;
SPREAD_PHEROMONE=0.2;
BASE_PHEROMONE=0.8;
global distances;
global pheromones;
global routes;
global variances;

# Get every available path for a given position
def getPaths(position, points, distances):
    paths = [];
    for point in points:
        if (distances[position, point] != 0 and distances[position, point] != None):
            paths.append(point);
    return paths;

# Travel of an ant on every point
def travel(ant):
    for x in range(NODES_NB):
        ant.checkedPoints.append(ant.currentPosition);

        # Handle final checkPoint
        if (x != NODES_NB - 1):
            # Retrieve all available paths
            availablePaths = getPaths(ant.currentPosition, ant.remainingPoints, ant.antDistances);
            gradedPaths = [];
            probabilities = [];
            totalGrades = 0;

            # Give grade for every path
            for path in availablePaths:
                pathGrade = (pheromones[ant.currentPosition, path] * PHEROMONE_RATE) * ((1 / ant.antDistances[ant.currentPosition, path]) * GRADE_MULTIPLIER);
                totalGrades += pathGrade;
                gradedPaths.append([path, pathGrade]);

            # Give percentage of chance to be taken
            for path in gradedPaths:
                probabilities.append(path[1] / totalGrades);

            # Choose Path
            choosenPath = rand.choices(population=availablePaths, weights=probabilities, k=1);

            # Remove visited checkPoint from Paths
            for point in ant.remainingPoints:
                if point == ant.currentPosition:
                    ant.remainingPoints = np.delete(ant.remainingPoints, np.where(ant.remainingPoints == point));

            ant.currentPosition = choosenPath[0]; 

    # Come back and spread pheromones
    for path in range(len(ant.checkedPoints) - 1):
        pheromones[ant.checkedPoints[path],ant.checkedPoints[path + 1]] += SPREAD_PHEROMONE ;
        pheromones[ant.checkedPoints[path + 1],ant.checkedPoints[path]] += SPREAD_PHEROMONE ;

    return ant.checkedPoints;         

# Make pheromnes evaporate
def evaporate(pheromone):
    return pheromone * (1 - EVAPORATION_RATE);

def init():
    # Determine that we use the global variables defined above
    global distances;
    global pheromones;
    global routes;
    global variances;

    # Initialize distances and pheromones matrixes
    distances = np.zeros((NODES_NB, NODES_NB));
    pheromones = np.full((NODES_NB, NODES_NB), BASE_PHEROMONE);
    routes = [];
    variances = [];

    # Set random distance for every city
    for x in range(NODES_NB):
        for y in range (NODES_NB):
            if x != y:
                distances[x, y] = distances[y, x] = rand.randrange(1, MAX_DISTANCE);

    # Start exploration
    for ant in range(POPULATION):
        
        # Create Ant
        ant = Ant(NODES_NB, distances);

        # Travel until final node
        routes.append(travel(ant));

        # Check variance
        if (len(routes) >= 1):
            difference = 0;
            lastValues = zip(routes[len(routes) - 1], routes[len(routes) - 2])
            for list1_i, list2_i in lastValues:
                if(list1_i-list2_i != 0):
                    difference += 1;
            variances.append(difference);

        print(difference);
        # Evaporate pheromones
        evaporation = np.vectorize(evaporate)(pheromones);
        pheromones = evaporation;

    # Export variance graph
    line_chart = pygal.Line(show_dots=True, show_legend=False, stroke=True, fill=True)
    line_chart.title = 'Variance evolution'
    line_chart.x_title = 'Ants'
    line_chart.y_title = 'Variance'
    line_chart.add('Variance', variances)
    line_chart.render_to_file('bar_chart.svg');

    # Print Path Graph
    G=nx.Graph();
    for x in range(len(routes[len(routes) - 1]) - 1):
        G.add_edge(routes[len(routes) - 1][x], routes[len(routes) - 1][x + 1]);

    nx.draw(G, with_labels=True, font_weight='bold');
    plt.show()

if __name__ == "__main__":
    init()