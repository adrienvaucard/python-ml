import math
import numpy
import inquirer
import time
import pygal

from random import randrange, uniform, random, choice

from Material import *
from Arrow import *
from Scorpio import *
from Launch import *

CHANCE_TO_MUTATE = 0.1;
GRADED_RETAIN_PERCENT = 0.2;
CHANCE_RETAIN_NONGRATED = 0.05;
POPULATION_COUNT = 1000;
GENERATION_COUNT_MAX = 10000;
MAXIMUM_FITNESS = 100;
EXPERIMENT_ENVIRONMENT = 9.81; #Earth Gravity
print('Which energy (in joules) do you want to reach ?') ;
EXPECTED_ENERGY = input();
BEST_GRADED_PERCENTAGE = int(POPULATION_COUNT * GRADED_RETAIN_PERCENT);

# Get Random population
def getRandomPopulation():
    return [getRandomLaunch(getRandomScorpio(getRandomArrow()), EXPERIMENT_ENVIRONMENT) for _ in range(POPULATION_COUNT)]

# Get Individual Fitness
def getFitness(launch):
    fitness = 100 / (abs(float(EXPECTED_ENERGY) - float(launch.impactEnergy)) + 1);
    return fitness;

# Get average Fitness
def getAverageFitness(population):
    totalFitness = 0;
    for launch in population:
        totalFitness += getFitness(launch);
    return totalFitness / POPULATION_COUNT;

# Sort population
def gradePopulation(population):

    gradedPopulation = [];
    for launch in population:
        gradedPopulation.append((launch, getFitness(launch)))
    return sorted(gradedPopulation, key=lambda x: x[1], reverse=True)

# Make population evolve for 1 generation
def evolvePopulation(population):

    # Retrieve graded Population
    sortedPopulation = gradePopulation(population);
    averageFitness = 0;
    solution = [];
    gradedPopulation = [];

    # Give grade and get average grade
    for launch, fitness in sortedPopulation:
        averageFitness += fitness;
        gradedPopulation.append(launch);
        if fitness >= (MAXIMUM_FITNESS - 0.01): # Child will be near to perfect MAXIMUM_FITNESS but never equal
            solution.append(launch);
    averageFitness /= POPULATION_COUNT;

    # End the script when solution is found
    if solution:
        return population, averageFitness, solution

    # Keep Best graded launches
    parents = gradedPopulation[:BEST_GRADED_PERCENTAGE]

    # Add some random launches
    for launch in gradedPopulation[BEST_GRADED_PERCENTAGE:]:
        if random() < CHANCE_RETAIN_NONGRATED:
            parents.append(launch);

    # Random mutation in population
    for launch in parents:
        if random() < CHANCE_TO_MUTATE:
            launch = getRandomLaunch(getRandomScorpio(getRandomArrow()), EXPERIMENT_ENVIRONMENT);

    # Crossover
    desiredLength = POPULATION_COUNT - len(parents);
    children = []
    while len(children) < desiredLength:
        firstParent = choice(parents);
        secondParent = choice(parents);

        # Create new Arrow
        childArrow = Arrow(
            firstParent.scorpio.arrow.material,
            round(uniform(firstParent.scorpio.arrow.length, secondParent.scorpio.arrow.length), arrowData['length']['digits']),
            round(uniform(firstParent.scorpio.arrow.diameter, secondParent.scorpio.arrow.diameter), arrowData['diameter']['digits'])
        );

        # Create new Scorpio
        childScorpio = Scorpio(
            childArrow,
            secondParent.scorpio.material,
            round(uniform(firstParent.scorpio.armLength, secondParent.scorpio.armLength), scorpioData['armLength']['digits']),
            round(uniform(firstParent.scorpio.armBase, secondParent.scorpio.armBase), scorpioData['armBase']['digits']),
            round(uniform(firstParent.scorpio.armHeight, secondParent.scorpio.armHeight), scorpioData['armHeight']['digits']),
            round(uniform(firstParent.scorpio.stringLength, secondParent.scorpio.stringLength), scorpioData['stringLength']['digits']),
            round(uniform(firstParent.scorpio.angle, secondParent.scorpio.angle), scorpioData['angle']['digits'])
        );

        # Create new Launch
        childLaunch = Launch(
            childScorpio,
            EXPERIMENT_ENVIRONMENT
        )

        child = childLaunch;
        children.append(child);

    parents.extend(children);
    return parents, averageFitness, solution;


# Main program
def main():
    ##########################################
    #############---------------##############
    #############-PROGRAM START-##############
    #############---------------##############
    ##########################################

    # Generate initial population;
    population = getRandomPopulation();

    # Give grades to population
    averageFitness = getAverageFitness(population);

    # Evolve
    i = 0
    solution = None
    avgFitnessLog = []
    while not solution and i < GENERATION_COUNT_MAX:
        population, averageFitness, solution = evolvePopulation(population)
        print('Current average fitness: %.2f' % averageFitness, '/ %d' % MAXIMUM_FITNESS, '(Generation %d)' % i);
        avgFitnessLog.append(round(averageFitness, 2));
        i += 1

    # Print the final stats
    averageGrade = getAverageFitness(population);
    print('Final average fitness: %.2f' % averageFitness, '/ %d' % MAXIMUM_FITNESS);

    # Print the solution
    if solution:
        print('Solution found after %d generations with a fitness of %d.' % (i, getFitness(solution[0])));
        print('Ideal Scorpio: ');
        print('    - Scorpio material: ' + str(solution[0].scorpio.material.name));
        print('    - Scorpio arm Length: ' + str(solution[0].scorpio.armLength));
        print('    - Scorpio arm Base: ' + str(solution[0].scorpio.armBase));
        print('    - Scorpio arm Height: ' + str(solution[0].scorpio.armHeight));
        print('    - Scorpio string length: ' + str(solution[0].scorpio.stringLength));
        print('    - Scorpio angle: ' + str(solution[0].scorpio.angle));
        print('Ideal Arrow: ');
        print('    - Arrow material: ' + str(solution[0].arrow.material.name));
        print('    - Arrow length: ' + str(solution[0].arrow.length));
        print('    - Arrow diameter: ' + str(solution[0].arrow.diameter));

        print(avgFitnessLog);
        # Export Graph
        line_chart = pygal.Line(show_dots=True, show_legend=False, stroke=True, fill=True)
        line_chart.title = 'Fitness evolution'
        line_chart.x_title = 'Generations'
        line_chart.y_title = 'Fitness'
        line_chart.add('Fitness', avgFitnessLog)
        line_chart.render_to_file('bar_chart.svg')
    else:
        print('No solution found after %d generations.' % i)
        print('- Last population was:')
        for number, launch in enumerate(population):
            print(number, '->',  ''.join(launch));



if __name__ == '__main__':
    main()

