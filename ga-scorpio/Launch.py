import math

from Arrow import *
from Scorpio import *

class Launch:
    def __init__(self, scorpio, gravity):
        self.arrow = scorpio.arrow;
        self.scorpio = scorpio;
        self.gravity = gravity;
        self.V = math.sqrt((self.scorpio.K * math.pow(self.scorpio.movementLength, 2)) / self.arrow.mass);
        self.reach = (math.pow(self.V, 2) / self.gravity) * math.sin(2 * (self.scorpio.angle * (math.pi / 180)));
        self.impactEnergy = 1/2 * self.arrow.mass * math.pow(self.V, 2);

def getRandomLaunch(scorpio, gravity):
    launch = Launch(
        scorpio,
        gravity
    )

    return launch;